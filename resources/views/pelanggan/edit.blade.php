@extends('template.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit pelanggan</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{route('pelanggan.index') }}"> Back</a>
        </div>
    </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your
    input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('pelanggan.update',$pelanggan->id) }}" method="POST">
    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="nama" value="{{ $pelanggan->nama }}" class="form-control" placeholder="Name">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jenis Kelamin:</strong>
                <input type="text" name="jekel" value="{{ $pelanggan->jekel }}" class="form-control" placeholder="jenis kelamin">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" value="{{ $pelanggan->email }}" class="form-control" placeholder="email">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No HP:</strong>
                <input type="text" name="hp" value="{{ $pelanggan->hp }}" class="form-control" placeholder="No hp">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                <input type="text" name="alamat" value="{{ $pelanggan->alamat }}" class="form-control" placeholder="Jln...">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 textcenter">
            <button type="submit" class="btn btnprimary">Submit</button>
        </div>
    </div>
</form>
@endsection